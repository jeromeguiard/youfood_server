from django.conf.urls.defaults import patterns, include, url
from menu.api import * 
from order.api import *
#from images.api import *
from tastypie.api import Api
from users.api import *
# Uncomment the next two lines to enable the admin:
from order.views import *
from django.contrib import admin
admin.autodiscover()
from youfood.order.views import *
api= Api(api_name='v1')
api.register(OrderListResource())
api.register(MenuItemResource())
api.register(OrderDetailResource())
#api.register(ImageResource())
api.register(UserResource())
api.register(UserYoufoodResource())
api.register(ZoneResource())
#api.register(ImageResource())

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'hello.views.home', name='home'),
    # url(r'^hello/', include('hello.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    (r'^api/',include(api.urls)),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    (r'^kitchen/','youfood.order.views.kitchenPage'),
    (r'^chart/','youfood.order.views.item_pivot_chart_view'),
    (r'^static/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT }),
    (r'^refresh/$','youfood.order.views.refresh'),
    
)
