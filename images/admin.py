from django.contrib import admin
from imagekit.admin import AdminThumbnail
from youfood.images.models import Image


class ImageAdmin(admin.ModelAdmin):
    #list_display = ('__str__', 'admin_image' )
    list_display = ('__str__','admin_image','admin_medium')
#    fields = ('tiny', 'admin_medium')
  #  admin_thumbnail = AdminThumbnail(image_field='tiny')


admin.site.register(Image, ImageAdmin)

