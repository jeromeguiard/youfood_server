from tastypie.resources import *
from tastypie import fields
#from tastypie.authorization import Authorization
from youfood.images.models import *

class ImageResource(ModelResource):
    
    class Meta:
        meta = False
        resource_name = 'images'
        queryset = Image.objects.all() 

