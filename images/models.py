from django.db import models
from imagekit.models import ImageSpecField
#from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFill, Adjust

class Image(models.Model): 
    original_image = models.ImageField(upload_to='images')
    formatted_image = ImageSpecField(image_field='original_image', format='JPEG',
            options={'quality': 90})
    tiny = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
        ResizeToFill(62, 62)], image_field='original_image',
        format='PNG', options={'quality': 90})
    small = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
        ResizeToFill(99, 99)], image_field='original_image',
        format='PNG', options={'quality': 90})
    medium = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
        ResizeToFill(173, 173)], image_field='original_image',
        format='PNG', options={'quality': 90})
    large = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
        ResizeToFill(200, 200)], image_field='original_image',
        format='PNG', options={'quality': 90})

    def admin_image(self):
        return '<img src="%s"/>' % self.formatted_image
    admin_image.allow_tags = True

    def admin_medium(self):
        return '<img src="%s"/>' % self.medium

    def __unicode__(self):
        return self.original_image.name
