from django.db import models
#from youfood.images.models import Image
# Create your models here.
#class menu(models.Model):
from imagekit.models import ImageSpecField
#from imagekit.models.fields import ImageSpecField
from imagekit.processors import ResizeToFill, Adjust
import os
from taggit.managers import TaggableManager

class menuItem(models.Model):
#    id = models.AutoField(primary_key = True)
    name = models.CharField(max_length=255,blank=False)
    description = models.TextField(max_length=1000,blank=False)
    price = models.FloatField(blank=False)
    preparationTime = models.FloatField(blank=True,null=True)
    photo = models.ImageField(upload_to='menu/youfood_' ,null=True,blank=True)
    large = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
        ResizeToFill(200, 200)], image_field='photo',
        format='PNG', options={'quality': 90},cache_to='youfood_')
    small = ImageSpecField([Adjust(contrast=1.2, sharpness=1.1),
        ResizeToFill(99, 99)], image_field='photo',
        format='PNG', options={'quality': 90}, cache_to='youfood_')
    tags = TaggableManager()
#    def get_upload_to(self, attname):
#        return 'menu/youfood_%d' % self.id

    def save(self):
        super(menuItem , self).save()
        print self.id
        os.rename(self.photo._get_path(),'/srv/www/youfood/media/menu/youfood_'+str(self.id)+".jpeg")
  #      self.photo = ''
 #       self.photo = models.ImageField(os.open('/srv/www/youfood/media/menu/youfood_'+str(self.id)+'.jpeg',0777),'youfood_6')
        #self.photo = models.ImageField(upload_to='menu/youfood_' ,null=True,blank=True)
        #ff = models.FileField('/srv/www/youfood/media/menu/youfood_'+str(self.id))
        #ph = models.ImageField(ff)
        #self.photo._set_file = open('/srv/www/youfood/media/menu/youfood_'+str(self.id))
        #self.photo = ph
#        super(menuItem, self).save()

    def cache_to(self, attname):
        return 'youfood_%d' % self.id

  #  def save(self) :
  #      super(menuItem,self).save()
  #      if self.photo:
            

    def __unicode__(self):
        return self.name
