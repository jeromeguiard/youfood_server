from django.contrib import admin
from youfood.menu.models import *
#from youfood.images.models import * 

class MenuItemAdmin(admin.ModelAdmin):
    fields = ('name','description','price','tags','photo')
    title = 'name'
admin.site.register(menuItem, MenuItemAdmin)
