# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding M2M table for field photos on 'menuItem'
        db.create_table('menu_menuitem_photos', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('menuitem', models.ForeignKey(orm['menu.menuitem'], null=False)),
            ('image', models.ForeignKey(orm['images.image'], null=False))
        ))
        db.create_unique('menu_menuitem_photos', ['menuitem_id', 'image_id'])


    def backwards(self, orm):
        
        # Removing M2M table for field photos on 'menuItem'
        db.delete_table('menu_menuitem_photos')


    models = {
        'images.image': {
            'Meta': {'object_name': 'Image'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'original_image': ('django.db.models.fields.files.ImageField', [], {'max_length': '100'})
        },
        'menu.menuitem': {
            'Meta': {'object_name': 'menuItem'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photos': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['images.Image']", 'symmetrical': 'False', 'blank': 'True'}),
            'preparationTime': ('django.db.models.fields.FloatField', [], {}),
            'price': ('django.db.models.fields.FloatField', [], {})
        }
    }

    complete_apps = ['menu']
