from tastypie.resources import *
from tastypie import fields
#from tastypie.authorization import Authorization
from youfood.menu.models import menuItem
from tastypie.constants import ALL, ALL_WITH_RELATIONS
###
from tastypie.fields import ListField
###

class MenuItemResource(ModelResource):
    tags = ListField()    
    
    class Meta:
        meta = False
        resource_name = 'menuItem'
        queryset = menuItem.objects.all()
#        include_resource_uri = False 
#        authorization = Authorization()
        allowed_methods = ['get','post','put']
 #       filtering ={
 #           "name":ALL,
 #           "tags":ALL,
 #       }
    def build_filters(self, filters=None):
        if filters is None:
            filters = {}

        orm_filters = super(MenuItemResource, self).build_filters(filters)

        if 'tag' in filters:
            orm_filters['tags__name__in'] = filters['tag'].split(',')
        return orm_filters

        
    def dehydrate_tags(self, bundle):
        return map(str, bundle.obj.tags.all())


    def save_m2m(self, bundle):
        tags = bundle.data.get('tags', [])
        bundle.obj.tags.set(*tags)
        return super(TaggedResource, self).save_m2m(bundle)
