from django.db import models
from django.contrib.auth.models import User
from tastypie.models import ApiKey
# Create your models here

class UserYoufood(models.Model):
    user = models.OneToOneField(User)
    key = models.CharField(blank=True,null=True,
                           max_length=80,help_text='Key from tastypie')
    ip = models.CharField(blank=True,null=True,
                          max_length=80,help_text='Last ip connected')
    is_staff = models.BooleanField()
    #waiter = models.ForeignKey('UserYoufood',null=True,blank=True,
         #                      help_text='Enter in that field the waiter')
    zone = models.ForeignKey('Zone', null=True,blank=True,
                             help_text='Zone of the table')#,default=youfood.users.models.Zone.objects.get(id=2))
    def save(self):
        self.key = ApiKey.objects.get(user = self.user.id).key
        self.is_staff = self.user.is_staff
        super(UserYoufood,self).save()
    def __unicode__(self):
        return self.user.username


class Zone(models.Model):
    zone_number = models.IntegerField(blank=True,null=True)
    waiter = models.OneToOneField('UserYoufood',related_name="waiter zone",null=True,blank=True,
                               help_text='Enter in that field the waiter')
    def __unicode__(self):
        return "zone"+str(self.zone_number) 
