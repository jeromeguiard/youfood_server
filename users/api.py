from tastypie.resources import *
from tastypie import fields
from tastypie.authorization import Authorization , DjangoAuthorization
from tastypie.authentication import BasicAuthentication,ApiKeyAuthentication
#from youfood.order.models import * 
#from django.contrib.auth.models import User
#from tastypie.models import create_api_key
#from tastypie.models import ApiKey
from youfood.users.models import *


class UserResource(ModelResource):
    class Meta:
        queryset = User.objects.all()
        resource_name = 'user'
        excludes = ['date_joined','email','first_name','is_active','is_staff','is_superuser','password','last_login','last_name','id']
        include_resource_uri = False

class UserYoufoodResource(ModelResource):
    user = fields.ToOneField('youfood.users.api.UserResource','user',full=True) 
    zone = fields.ToOneField('youfood.users.api.ZoneResource','zone',full=True) 
    class Meta:
        queryset = UserYoufood.objects.all()
        resource_name = 'userDetail'
        include_resource_uri = False
        allowed_method= ['put','get','post']
        authorization = Authorization()

class ZoneResource(ModelResource):
    class Meta:
        queryset= Zone.objects.all()
        resource_name = 'zone'
        excludes=['waiter','id']
        include_resource_uri = False
