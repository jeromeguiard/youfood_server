from django.db import models
from youfood.menu.models import menuItem
from youfood.users.models import UserYoufood
from .signals import *

class orderDetail(models.Model):
    item = models.ForeignKey(menuItem)
    quantity = models.IntegerField(default= 1 , blank = False)
    time = models.DateTimeField(auto_now_add = True)
 #   cook_by = models.ForeignKey(UserYoufood,null=True)
 #   start_cook_time = models.DateTiemField(auto_now_add=True)
    def __unicode__(self):
        return self.item.name

class orderList(models.Model):
    PAYMENT_METHOD = (
        ('D','Debit card'),
        ('S','Money supply'),
        ('C','Cheque'),
    )
    userYoufood = models.ForeignKey(UserYoufood)
    listItems = models.ManyToManyField(orderDetail)
    time_order = models.DateTimeField(auto_now_add=True)
    validated = models.BooleanField(default=False)
 #   validated_by = models.ForeignKey(UserYoufood,null=True)
 #   total_order = models.FloatField(self.__calcul_total_amount__)
 #   paid = models.BooleanField(default=False)
  #  payment_method = models.CharField(max_length=1 , choices=PAYMENT_METHOD)
    def __unicode__(self):
        return "Order From "+self.userYoufood.user.username +"  "+ str(self.id)

    def __calcul_total_amount__(self):
        total = 0 ;
        for item in self.listItems.all():
            total += item.item.price
        return total

post_save.connect(print_order_infos,sender=orderList)
