# Create your views here.
from django.shortcuts import get_object_or_404, render_to_response
from django.http import HttpResponseRedirect, HttpResponse
from .models import *
from youfood.users.models import UserYoufood
import datetime
from django.contrib.auth.models import User
from chartit import PivotDataPool, PivotChart
from django.db.models import Avg

def refresh(request):
    p = orderDetail.objects.all()
    datas = []
    for o in p: 
        datas.append(o.item)
    return render_to_response('refresh.html', {'datas':datas})

def kitchenPage(request) :
    t = datetime.datetime.utcnow()
    time_now = str(t.year)+","+str(t.month-1)+","+str(t.day)+","+str(t.hour+8)+","+str(t.minute-6)+","+str(t.second-10)+","+str(t.microsecond)
    last = orderDetail.objects.all().latest('id').id
    cookers = User.objects.filter(groups__name='Cookers')
    return render_to_response('pageKitchen.html', {'time_now': time_now,'lastid':last,'cookers':cookers})


def item_pivot_chart_view(request):
    itempivotdata = \
        PivotDataPool(
            series =
              [{'options': {
                 'source': orderDetail.objects.all(),
                 'categories':'time'},
                'terms':{
                  'number_ordered': Avg('quantity')}}#,
#                  'legend_by' : ['__unicode__()'],
                  #'top_n_per_cat':3 }}
               ])

    itempivcht = PivotChart(
            datasource= itempivotdata,
            series_options=
              [{'options':{
                   'type' : 'column',
                   'stacking':True},
                'terms':[
                   'number_ordered']}],
            chart_options =
             {'title':{
                  'text':"temp title"},
               'xAxis':{
                  'title': {
                      'text':'time'}}})
    return render_to_response('chart.html',{'itempivchart':itempivcht})
