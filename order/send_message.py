import socket
import sys

def sendmessage(instance):
    HOST, PORT = instance.userYoufood.zone.waiter.ip, 9999
    data = instance.userYoufood.user.username + " ," + str(instance.id)
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        if(instance.validated == False):
            sock.connect((HOST, PORT))
            sock.sendall(data + "\n")
    except:
        print "can't connect" 
    finally:
        sock.close()
