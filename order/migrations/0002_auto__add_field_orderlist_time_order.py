# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'orderList.time_order'
        db.add_column('order_orderlist', 'time_order', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, default=datetime.date(2012, 4, 6), blank=True), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'orderList.time_order'
        db.delete_column('order_orderlist', 'time_order')


    models = {
        'menu.menuitem': {
            'Meta': {'object_name': 'menuItem'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'preparationTime': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        'order.orderdetail': {
            'Meta': {'object_name': 'orderDetail'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menu.menuItem']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'order.orderlist': {
            'Meta': {'object_name': 'orderList'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'listItems': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['order.orderDetail']", 'symmetrical': 'False'}),
            'table_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'time_order': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['order']
