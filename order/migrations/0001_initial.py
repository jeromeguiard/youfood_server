# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'orderDetail'
        db.create_table('order_orderdetail', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('item', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['menu.menuItem'])),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(default=1)),
        ))
        db.send_create_signal('order', ['orderDetail'])

        # Adding model 'orderList'
        db.create_table('order_orderlist', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('table_number', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
        ))
        db.send_create_signal('order', ['orderList'])

        # Adding M2M table for field listItems on 'orderList'
        db.create_table('order_orderlist_listItems', (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('orderlist', models.ForeignKey(orm['order.orderlist'], null=False)),
            ('orderdetail', models.ForeignKey(orm['order.orderdetail'], null=False))
        ))
        db.create_unique('order_orderlist_listItems', ['orderlist_id', 'orderdetail_id'])


    def backwards(self, orm):
        
        # Deleting model 'orderDetail'
        db.delete_table('order_orderdetail')

        # Deleting model 'orderList'
        db.delete_table('order_orderlist')

        # Removing M2M table for field listItems on 'orderList'
        db.delete_table('order_orderlist_listItems')


    models = {
        'menu.menuitem': {
            'Meta': {'object_name': 'menuItem'},
            'description': ('django.db.models.fields.TextField', [], {'max_length': '1000'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'photo': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'preparationTime': ('django.db.models.fields.FloatField', [], {'null': 'True', 'blank': 'True'}),
            'price': ('django.db.models.fields.FloatField', [], {})
        },
        'order.orderdetail': {
            'Meta': {'object_name': 'orderDetail'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'item': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['menu.menuItem']"}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'default': '1'})
        },
        'order.orderlist': {
            'Meta': {'object_name': 'orderList'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'listItems': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['order.orderDetail']", 'symmetrical': 'False'}),
            'table_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['order']
