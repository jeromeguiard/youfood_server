from tastypie.resources import *
from tastypie import fields
from tastypie.authorization import Authorization , DjangoAuthorization
from tastypie.authentication import BasicAuthentication ,ApiKeyAuthentication
from youfood.order.models import * 
from django.contrib.auth.models import User
from tastypie.models import create_api_key
from tastypie.models import ApiKey
from youfood.users.api import UserYoufoodResource

class OrderDetailResource(ModelResource):
    item = fields.ToOneField('youfood.menu.api.MenuItemResource' ,'item',full=True)
    class Meta:
        meta = False
        resource_name = 'orderDetail'
        queryset = orderDetail.objects.all()        
        include_resource_uri = False
        allowed_method = ['get','post','put']
        include_resource_uri = False
#        excludes = ['id']
#        authorization = Authorization()        

class OrderListResource(ModelResource):
    listItems = fields.ToManyField('youfood.order.api.OrderDetailResource','listItems',full=True)
    userYoufood = fields.ToOneField(UserYoufoodResource,'userYoufood',null=True,full=False)
    class Meta : 
        meta = False
        resource_name='orderList'
        queryset = orderList.objects.all()
        allowed_method = ['get','post','put']
        authentication = ApiKeyAuthentication()
        authorization = Authorization()
        include_resource_uri = False
        filtering = {
            "validated": ALL, 
        }
